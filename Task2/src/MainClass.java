import javax.swing.JOptionPane;
import java.util.Arrays;
import java.util.Date;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(args[0]);
		System.out.println(args[1] + " | " + args[2]);

		if (args.length < 3) {
			System.err.println("Incorrenct agrs number!");
		} else {
			System.out.println(" * " + args[0] + " ** " + args[1] + " *** " + args[2]);
		}

		for (int i = 0; i < args.length; i++) {
			System.out.println(args[i]);
		}

		int val1 = Integer.parseInt(args[4]);
		int val2 = Integer.parseInt(args[6]);

		switch (args[5]) {
		case "*": {
			System.out.println(val1 * val2);
			break;
		}
		case "+": {
			System.out.println(val1 + val2);
			break;
		}
		case "-": {
			System.out.println(val1 - val2);
			break;
		}
		case "/": {
			System.out.println(val1 / val2);
			break;
		}
		}

		int arr1[] = new int[10];
		for (int i = 1; i < 10; i++) {
			arr1[i] = 5;
		}

		for (int i = 1; i < 10; i++) {
			System.out.print(arr1[i] + " ");
		}

		System.out.println("");

		int arr2[] = new int[10];

		for (int i = 1; i < 10; i++) {
			arr2[i] = 2 * i + 1;
			System.out.print(arr2[i] + " ");
		}

		System.out.println("");

		int array_size = 15;
		int arr3[] = new int[array_size];
		for (int i = 1; i < array_size; i++) {
			arr3[i] = 20 - i * 2;
			System.out.print(arr3[i] + " ");
		}
		System.out.println("");

		String txt1 = JOptionPane.showInputDialog("Insert array size");
		int array_size2 = Integer.parseInt(txt1);

		int array_rand = Integer.parseInt(args[3]);

		int arr4[] = new int[array_size2];

		for (int i = 1; i < array_size2; i++) {
			arr4[i] = (int) (Math.random() * array_rand);
		}

		for (int i = 1; i < array_size2; i++) {
			System.out.print(arr4[i] + " ");
		}

		System.out.println("");

		int retarr[] = new int[arr4.length];
		for (int i = 0; i < arr4.length; i++) {
			retarr[i] = arr4[i];
		}

		// Date startTime = new Date();
		long start = System.nanoTime();
		int sorted_array[] = boubleSort(retarr);
		long end = System.nanoTime();
		// Date endTime = new Date();

		// System.out.println("Time " + (endTime.getTime() - startTime.getTime()) +
		// "ms");
		System.out.println("Time " + (end - start) + " ns");

		for (int i = 1; i < array_size2; i++) {
			System.out.print(sorted_array[i] + " ");
		}
		System.out.println("");

		// startTime = new Date();
		start = System.nanoTime();
		Arrays.sort(arr4);
		end = System.nanoTime();
		// endTime = new Date();

		// System.ou`t.println("Time " + (endTime.getTime() - startTime.getTime()) +
		// "ms");
		System.out.println("Time " + (end - start) + " ns");

		for (int i = 1; i < array_size2; i++) {
			System.out.print(arr4[i] + " ");
		}
	}

	public static int[] boubleSort(int[] retarr) {
		int temp = 0;
		for (int i = 0; i < retarr.length - 1; i++) {
			for (int j = 1; j < retarr.length - i; j++) {
				if (retarr[j - 1] > retarr[j]) {
					temp = retarr[j];
					retarr[j] = retarr[j - 1];
					retarr[j - 1] = temp;
				}
			}
		}
		return retarr;
	}

}
