import java.io.File;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;

public class SaveLoadXML {
	public static void save(String filePath, AddressesBook bookLib) {
		try {
			File file = new File(filePath);
			JAXB.marshal(bookLib, file);
		}
		catch (DataBindingException io) {
			System.err.println(io.getMessage());
		}
		catch (Exception se) {
			System.err.println("Unexpected exception " + se.getMessage() + "\n" + se.getStackTrace()[0]);
		}
	}
	
	public static AddressesBook load(String filePath) {
		try {
			File file = new File(filePath);
			return JAXB.unmarshal(file, AddressesBook.class);
		}
		catch (DataBindingException io) {
			System.err.println(io.getMessage());
		}
		catch (Exception se) {
			System.err.println("Unexpected exception " + se.getMessage() + "\n" + se.getStackTrace()[0]);
		}
		return new AddressesBook();
	}
}
