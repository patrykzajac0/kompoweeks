import javax.xml.bind.*;

public class MainClass {

	public static void main(String[] args) {
		AddressesBook bookLib = new AddressesBook();
		
		bookLib = SaveLoadXML.load("file.xml");
		SaveLoadXML.save("data.xml", bookLib);
	}

}
