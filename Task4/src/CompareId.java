import java.util.Comparator;

public class CompareId implements Comparator<Address> {

	@Override
	public int compare(Address a, Address b) {
		// TODO Auto-generated method stub
		return a.getId() < b.getId() ? -1 : a.getId() == b.getId() ? 0 : 1;
	}

}
