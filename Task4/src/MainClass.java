
public class MainClass {

	public static void main(String[] args) {
		AddressesBook bookLib = new AddressesBook();
		int size = 10;
		try {
			for (int i = 0; i < size; i++) {
				Address temp = new Address("Street", Integer.toString(i), "ZipCode", "City");
				bookLib.add(temp);
			}
		} catch (IndexOutOfBoundsException ex) {
			System.out.println(ex.getMessage());
		}
		bookLib.sort(Address.AddressField.number);
		for (int i = 0; i < bookLib.size(); i++) {
			System.out.println(bookLib.get(i).toString());
		}
	}

}
