import java.util.Comparator;

public class CompareCity implements Comparator<Address> {

	@Override
	public int compare(Address o1, Address o2) {
		// TODO Auto-generated method stub
		return o1.getCity().compareTo(o2.getCity());
	}

}
