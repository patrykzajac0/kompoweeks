import java.io.*;
import java.util.Arrays;

public class MainClass {

	public static void main(String[] args) {

		File file = new File("file1.txt");

		String stringToSave = new String("ABCDEFGHIJ");

		try {
			file.createNewFile();
			FileWriter fw = new FileWriter(file);
			fw.write(stringToSave, 0, 7);
			fw.close();
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		char buffor[] = new char[10];

		try {
			FileReader fr = new FileReader(file);
			fr.read(buffor, 0, 7);
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		String readedString = new String(buffor);
		System.out.println("Readed string: " + readedString);

		int range = 30;
		int i;
		int size = 15;
		int arr[] = new int[size];
		for (i = 0; i < size; i++) {
			arr[i] = (int) (Math.random() * range);
		}

		try {
			DataOutputStream ds = new DataOutputStream(new FileOutputStream("file.bin"));
			for (i = 0; i < size; i++) {
				ds.writeInt(arr[i]);
			}
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		int arrRead[] = new int[size];

		try {
			DataInputStream ds = new DataInputStream(new FileInputStream("file.bin"));
			for (i = 0; i < size; i++) {
				arrRead[i] = ds.readInt();
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}
		Arrays.sort(arrRead);
		for (i = 0; i < size; i++) {
			System.out.print(arrRead[i] + " ");
		}

	}

}
