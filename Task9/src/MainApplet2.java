import java.applet.Applet;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.TextField;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class MainApplet2 extends Applet implements ItemListener {
	boolean bokA = false;
	boolean bokB = false;
	boolean bokC = false;
	int kolor = 0;
	int rozmiar = 18;
	String czcionka = "Times New Roman";
	String txt = "TEKST probny JAVA";
	byte bold = 0;
	byte ital = 0;
	Button przycisk1;
	Button przycisk2;
	Button przycisk3;
	Checkbox radio1;
	Checkbox radio2;
	Checkbox radio3;
	Checkbox radio4;
	CheckboxGroup group = new CheckboxGroup();
	TextField oknoTekstowe;
	Choice choice1;
	Choice color;
	Checkbox checkItal;
	Checkbox checkBold;

	public void paint(Graphics g) {

		Font appFont = new Font(this.czcionka, this.ital | this.bold, this.rozmiar);
		if (this.kolor == 0) {
			g.setColor(Color.black);
		}

		if (this.kolor == 1) {
			g.setColor(Color.red);
		}

		if (this.kolor == 2) {
			g.setColor(Color.green);
		}

		if (this.kolor == 3) {
			g.setColor(Color.blue);
		}
		g.setFont(appFont);
		g.drawString(this.txt, 10, 100);
	}

	public void itemStateChanged(ItemEvent evt) {
		if (evt.getSource() == this.radio1) {
			this.rozmiar = 24;
		}

		if (evt.getSource() == this.radio2) {
			this.rozmiar = 32;
		}

		if (evt.getSource() == this.radio3) {
			this.rozmiar = 40;
		}

		if (evt.getSource() == this.radio4) {
			this.rozmiar = 48;
		}

		if (evt.getSource() == this.checkItal) {
			if (this.ital == 0) {
				this.ital = 2;
			} else {
				this.ital = 0;
			}
		}

		if (evt.getSource() == this.checkBold) {
			if (this.bold == 0) {
				this.bold = 1;
			} else {
				this.bold = 0;
			}
		}

		if (evt.getItemSelectable() == this.choice1) {
			this.czcionka = new String(((Choice) evt.getItemSelectable()).getSelectedItem());
		}
		if (evt.getItemSelectable() == this.color) {
			this.kolor = Integer.valueOf(((Choice) evt.getItemSelectable()).getSelectedIndex());
		}

		this.repaint();
	}

	public void init() {
		this.setSize(500, 500);
		this.przycisk1 = new Button("Test");
		this.add(this.przycisk1);
		this.przycisk1.addMouseListener(new MouseAdapter());
		this.radio1 = new Checkbox("18pt", true, this.group);
		this.add(this.radio1);
		this.radio1.addItemListener(this);
		this.radio2 = new Checkbox("24pt", false, this.group);
		this.add(this.radio2);
		this.radio2.addItemListener(this);
		this.radio3 = new Checkbox("32pt", false, this.group);
		this.add(this.radio3);
		this.radio3.addItemListener(this);
		this.radio4 = new Checkbox("36pt", false, this.group);
		this.add(this.radio4);
		this.radio4.addItemListener(this);
		this.checkItal = new Checkbox("Italic");
		this.add(this.checkItal);
		this.checkItal.addItemListener(this);
		this.checkBold = new Checkbox("Bold");
		this.add(this.checkBold);
		this.checkBold.addItemListener(this);
		this.choice1 = new Choice();
		this.choice1.add("Times New Roman");
		this.choice1.add("Arial");
		this.choice1.add("Courier New");
		this.choice1.add("Tahoma");
		this.choice1.add("Verdana");
		this.choice1.add("Lucida");
		this.choice1.add("Comic");
		this.add(this.choice1);
		this.choice1.addItemListener(this);
		this.color = new Choice();
		this.color.add("Black");
		this.color.add("Red");
		this.color.add("Green");
		this.color.add("Blue");
		this.add(this.color);
		this.color.addItemListener(this);
	}
}