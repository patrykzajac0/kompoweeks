import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DecimalFormat format = new DecimalFormat("#0.00");

		System.out.println("Hello world!");
		double a = 4;
		double b = 5;
		double c = 7;
		System.out.println(a + b);
		if (b != 0) {
			System.out.println(format.format(a / b));
		} else {
			System.out.println("Do not divide by zero!");
		}
		System.out.println(a + c);
		System.out.println(b - c);
		if (c != 0) {
			System.out.println(a / c);
		} else {
			System.out.println("Do not divide by zero!");
		}

		String txt1;
		txt1 = JOptionPane.showInputDialog("Insert first value");

		String txt2;
		txt2 = JOptionPane.showInputDialog("Insert secound value");

		double val1 = Double.parseDouble(txt1);
		int val2 = Integer.parseInt(txt2);

		System.out.println(format.format(val1 + val2));

		String countTxt = JOptionPane.showInputDialog("Insert number of rows");
		int count = Integer.parseInt(countTxt);

		method1(count);
		method2(count);
	}

	public static void method1(int count) {
		int row[][] = new int[count][];

		row[0] = new int[1];
		row[0][0] = 1;

		for (int i = 1; i < count; i++) {
			row[i] = new int[i + 1];
			row[i][0] = 1;
			row[i][i] = 1;
			for (int j = 1; j < i; j++) {
				row[i][j] = row[i - 1][j - 1] + row[i - 1][j];
			}
		}
		for (int i = 0; i < count; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(row[i][j] + " ");
			}
			System.out.println("");
		}

	}

	public static void method2(int count) {
		for (int i = 1; i < count + 1; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(newton(i, j) + " ");
			}
			System.out.println("");
		}
	}

	public static int newton(int val1, int val2) {
		int result = 1;
		for (int i = val2; i < val1; i++) {
			result *= i;
		}
		for (int i = 1; i < val1 - val2 + 1; i++) {
			result /= i;
		}

		return result;
	}

}
