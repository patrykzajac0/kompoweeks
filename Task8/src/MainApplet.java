import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MainApplet extends Applet implements ActionListener, ItemListener {

	Button btn1, btn2;
	TextField txt, txt2;
	Checkbox cb1, cb2;
	Checkbox cbg1, cbg2;

	CheckboxGroup cbg;

	Choice ch;

	Button btnMenu1, btnMenu2;
	menuFrame fullmenuwindow;

	public void init() {

		txt = new TextField(20);
		add(txt);

		btn1 = new Button("Welcome");
		add(btn1);
		btn1.addActionListener(this);

		btn2 = new Button("to JAVA!!!");
		add(btn2);
		btn2.addActionListener(this);

		txt2 = new TextField(20);
		add(txt2);

		cb1 = new Checkbox("1");
		add(cb1);
		cb1.addItemListener(this);

		cb2 = new Checkbox("2");
		add(cb2);
		cb2.addItemListener(this);

		cbg = new CheckboxGroup();

		cbg1 = new Checkbox("1 group", false, cbg);
		add(cbg1);
		cbg1.addItemListener(this);

		cbg2 = new Checkbox("2 group", false, cbg);
		add(cbg2);
		cbg2.addItemListener(this);

		ch = new Choice();
		ch.add("Value 1");
		ch.add("Value 2");

		add(ch);
		ch.addItemListener(this);

		btnMenu1 = new Button("Show full menu window");
		add(btnMenu1);
		btnMenu1.addActionListener(this);
		btnMenu2 = new Button("Hide full menu window");
		add(btnMenu2);
		btnMenu2.addActionListener(this);

		fullmenuwindow = new menuFrame("Full menus");
		fullmenuwindow.setSize(100, 100);
	}

	public void paint(Graphics g) {

		/*
		 * g.drawLine(100, 10, 10, 150); g.drawLine(10, 150, 150, 150); g.drawLine(150,
		 * 150, 100, 10);
		 * 
		 * 
		 * resize(500,500);
		 * 
		 * g.setColor(Color.green);
		 * 
		 * 
		 * g.drawRect(10,210,120,120); g.drawRect(20,220,100,100);
		 * g.drawRect(30,230,80,80);
		 * 
		 * 
		 * 
		 * g.setColor(Color.blue); g.drawRoundRect(100, 300, 70, 70, 30, 30);
		 * 
		 * 
		 * g.setColor(Color.yellow); g.drawOval(250, 50, 80, 40);
		 * 
		 * 
		 * Image image = getImage(getCodeBase(), "welcome.jpg"); g.drawImage(image, 200,
		 * 320, 150, 150, this);
		 * 
		 * String s = new String ("Hello World !!!"); Font appFont = new Font("Arial",
		 * Font.BOLD, 28);
		 * 
		 * g.setColor(Color.black); g.setFont(appFont); g.drawString(s,250, 300);
		 */
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btn1) {
			txt.setText("Welcome");
		} else if (arg0.getSource() == btn2) {
			txt.setText("to JAVA!!!");
		} else if (arg0.getSource() == btnMenu1) {
			fullmenuwindow.setVisible(true);
		}
		if (arg0.getSource() == btnMenu2) {
			fullmenuwindow.setVisible(false);
		}

	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		if (arg0.getItemSelectable() == cb1) {
			txt2.setText("Check box 1 clicked!");
		} else if (arg0.getItemSelectable() == cb2) {
			txt2.setText("Check box 2 clicked!");
		} else if (arg0.getItemSelectable() == cbg1) {
			txt2.setText("Check box 1 group clicked!");
		} else if (arg0.getItemSelectable() == cbg2) {
			txt2.setText("Check box 2 group clicked!");
		} else if (arg0.getItemSelectable() == ch) {
			txt2.setText(((Choice) arg0.getItemSelectable()).getSelectedItem());
		}
	}

}

class menuFrame extends Frame implements ActionListener {

	Menu Menu1, SubMenu1;
	MenuBar Menubar1;
	TextField text1;
	MenuItem menuitem1, menuitem2, menuitem4;
	CheckboxMenuItem menuitem3;

	menuFrame(String title) {
		super(title);
		text1 = new TextField("Full menu");
		setLayout(new GridLayout(1, 1));
		add(text1);
		Menubar1 = new MenuBar();
		Menu1 = new Menu("File");

		menuitem1 = new MenuItem("Item 1");
		menuitem1.addActionListener(this);
		Menu1.add(menuitem1);

		menuitem2 = new MenuItem("Item 2");
		menuitem2.addActionListener(this);
		Menu1.add(menuitem2);

		Menu1.addSeparator();

		menuitem3 = new CheckboxMenuItem("Check Item");
		menuitem3.addActionListener(this);
		Menu1.add(menuitem3);

		Menu1.addSeparator();

		SubMenu1 = new Menu("Sub menus");
		SubMenu1.add(new MenuItem("Hello"));
		SubMenu1.add(new MenuItem("from"));
		SubMenu1.add(new MenuItem("Java"));

		Menu1.add(SubMenu1);
		Menubar1.add(Menu1);
		setMenuBar(Menubar1);

		Menu1.addSeparator();

		menuitem4 = new MenuItem("Exit");
		menuitem4.addActionListener(this);
		Menu1.add(menuitem4);
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == menuitem1) {
			text1.setText("Item 1");
		}
		if (event.getSource() == menuitem2) {
			menuitem2.setEnabled(false);
			text1.setText("Item 2");
		}
		if (event.getSource() == menuitem3) {
			((CheckboxMenuItem) event.getSource()).setState(true);
		}
		if (event.getSource() == menuitem4) {
			setVisible(false);
		}

	}
}
