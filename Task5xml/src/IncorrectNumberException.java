
public class IncorrectNumberException extends Exception {
	private static final long serialVersionUID = -2357826053749747504L;

	public IncorrectNumberException(String message) {
		super(message);
	}
}
