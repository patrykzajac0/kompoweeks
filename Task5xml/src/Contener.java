import java.util.LinkedList;

public interface Contener<T> {
	void add(T object);
	void remove(int index) throws Exception;
	LinkedList<T> getAll();
	T get(int index);
}