import java.util.Collections;
import java.util.LinkedList;


public class AddressesBook implements Contener<Address>{
	private LinkedList<Address> addresses;

	public AddressesBook() {
		addresses = new LinkedList<Address>();
	}
	
	public AddressesBook(AddressesBook bookLib) {
		this.addresses = (LinkedList<Address>) bookLib.addresses.clone();
	}

	public LinkedList<Address> getAll() {
		return addresses;
	}

	public void setAddresses(LinkedList<Address> addresses) {
		this.addresses = addresses;
	}

	public void add(Address address) {
		addresses.add(address);
	}

	public Address get(int index) throws IndexOutOfBoundsException {
		if (index >= addresses.size() || index < 0) {
			throw new IndexOutOfBoundsException("Incorrect index");
		}
		return addresses.get(index);
	}

	public int size() {
		return addresses.size();
	}

	public void remove(int index) throws IndexOutOfBoundsException {
		if (index >= addresses.size() || index < 0) {
			throw new IndexOutOfBoundsException("Incorrect index");
		}
		addresses.remove(index);
	}

	public void sort(Address.AddressField field) {
		switch (field) {
		case id: {
			Collections.sort(addresses, new CompareId());
			// Arrays.sort(addresses, (a, b) -> a.getId() < b.getId() ? -1 : a.getId() ==
			// b.getId() ? 0 : 1);
		}
		case street: {
			Collections.sort(addresses, new CompareStreet());
			// Arrays.sort(addresses, (a, b) ->
			// a.getStreet().compareToIgnoreCase(b.getStreet()));
		}
		case number: {
			Collections.sort(addresses, new CompareNumber());
			// Arrays.sort(addresses, (a, b) ->
			// a.getNumber().compareToIgnoreCase(b.getNumber()));
		}
		case zipCode: {
			Collections.sort(addresses, new CompareZipCode());
			// Arrays.sort(addresses, (a, b) ->
			// a.getZipCode().compareToIgnoreCase(b.getZipCode()));
		}
		case city: {
			Collections.sort(addresses, new CompareCity());
			// Arrays.sort(addresses, (a, b) ->
			// a.getCity().compareToIgnoreCase(b.getCity()));
		}
		}
		// return addresses;
	}
}
