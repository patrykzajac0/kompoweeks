import java.io.Serializable;

public class Address implements Comparable<Address>, Serializable, ZipCode {

	private static final long serialVersionUID = 4152469418572107393L;
	private long id;
	private String street;
	private String number;
	private String zipCode;
	private String city;

	public Address() {
	}

	public Address(String street, String number, String zipCode, String city)
			throws IncorrectZipCodeLengthException, IncorrectNumberException {
		if (number.length() > 0) {
			try {
				Integer.parseInt(number.substring(0, 1));
			} catch (Exception ex) {
				throw new IncorrectNumberException("Number must begin with digit");
			}

			this.number = number;
		} else {
			throw new IncorrectNumberException("Number can't be empty");
		}
		if (zipCode.length() > 7) {
			throw new IncorrectZipCodeLengthException("ZipCode too long");
		}
		this.id = 0;
		this.street = street;
		this.number = number;
		this.zipCode = zipCode;
		this.city = city;
	}

	public Address(Address address) throws IncorrectZipCodeLengthException, IncorrectNumberException {
		if (address.number.length() > 0) {
			try {
				Integer.parseInt(address.number.substring(0, 1));
			} catch (Exception ex) {
				throw new IncorrectNumberException("Number must begin with digit");
			}
		} else {
			throw new IncorrectNumberException("Number can't be empty");
		}
		if (address.zipCode.length() > 7) {
			throw new IncorrectZipCodeLengthException("ZipCode too long");
		}
		this.id = 0;
		this.street = address.street;
		this.number = address.number;
		this.zipCode = address.zipCode;
		this.city = address.city;
	}

	public Address(Object object) throws IncorrectZipCodeLengthException, IncorrectNumberException {
		if (object instanceof Address) {
			if (((Address) object).getNumber().length() > 0) {
				try {
					Integer.parseInt(((Address) object).number.substring(0, 1));
				} catch (Exception ex) {
					throw new IncorrectNumberException("Number must begin with digit");
				}
			} else {
				throw new IncorrectNumberException("Number can't be empty");
			}
			if (((Address) object).getZipCode().length() > 7) {
				throw new IncorrectZipCodeLengthException("ZipCode too long");
			}
			this.id = 0;
			this.street = ((Address) object).getStreet();
			this.number = ((Address) object).getNumber();
			this.zipCode = ((Address) object).getZipCode();
			this.city = ((Address) object).getCity();
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) throws IncorrectNumberException {
		if (number.length() > 0) {
			try {
				Integer.parseInt(number.substring(0, 1));
			} catch (Exception ex) {
				throw new IncorrectNumberException("Number must begin with digit");
			}

			this.number = number;
		} else {
			throw new IncorrectNumberException("Number can't be empty");
		}
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) throws IncorrectZipCodeLengthException {
		if (zipCode.length() > 7) {
			throw new IncorrectZipCodeLengthException("ZipCode too long");
		}
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", number=" + number + ", zipCode=" + zipCode + ", city="
				+ city + "]";
	}

	@Override
	public int compareTo(Address element) {
		if (this.street.compareTo(element.street) == 0) {
			if (this.number.compareTo(element.number) == 0) {
				if (this.zipCode.compareTo(element.zipCode) == 0) {
					return this.city.compareTo(element.city);
				} else {
					return this.zipCode.compareTo(element.zipCode);
				}
			} else {
				return this.number.compareTo(element.number);
			}

		} else {
			return this.street.compareTo(element.street);
		}
	}

	public enum AddressField {
		id, street, number, zipCode, city
	}

}
