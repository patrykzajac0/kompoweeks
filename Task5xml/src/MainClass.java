import java.io.*;
import java.util.Arrays;

public class MainClass {

	public static void main(String[] args) {
		File file = new File("file1.txt");

		String stringToSave = new String("ABCDEFGHIJ");

		try {
			file.createNewFile();
			FileWriter fw = new FileWriter(file);
			fw.write(stringToSave, 0, 7);
			fw.close();
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		char buffor[] = new char[10];

		try {
			FileReader fr = new FileReader(file);
			fr.read(buffor, 0, 7);
			fr.close();
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		try {
			StringWriter readedString = new StringWriter();
			readedString.write(buffor, 0, buffor.length);
			System.out.println("Readed string: " + readedString);
			readedString.close();
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		int range = 30;
		int i;
		int size = 15;
		int arr[] = new int[size];
		byte barr[] = new byte[size];

		for (i = 0; i < size; i++) {
			arr[i] = (int) (Math.random() * range);
			barr[i] = (byte) (Math.random() * 255);
		}

		try {
			FileOutputStream fs = new FileOutputStream("file.bin");
			for (i = 0; i < size; i++) {
				fs.write(barr, 0, barr.length);
			}
			fs.close();
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		int arrRead[] = new int[size];
		byte barrRead[] = new byte[size];

		try {
			FileInputStream fs = new FileInputStream("file.bin");
			fs.read(barrRead);
			fs.close();
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		try {
			DataOutputStream ds = new DataOutputStream(new FileOutputStream("file.bin"));
			for (i = 0; i < size; i++) {
				ds.writeInt(arr[i]);
			}
			ds.close();
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		try {
			DataInputStream ds = new DataInputStream(new FileInputStream("file.bin"));
			for (i = 0; i < size; i++) {
				arrRead[i] = ds.readInt();
			}
			ds.close();
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}
		Arrays.sort(arrRead);
		for (i = 0; i < size; i++) {
			System.out.print(arrRead[i] + " ");
		}
		System.out.println();

		try {
			Address addr = new Address("StreetSer", "125", "123", "CitySet");
			try {
				FileOutputStream fileOut = new FileOutputStream("address.ser");
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(addr);
				out.close();
				fileOut.close();
			} catch (IOException io) {
				System.out.println(io.getMessage());
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}

			Address readed = new Address();
			try {
				FileInputStream fileIn = new FileInputStream("address.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				readed = new Address(in.readObject());
				in.close();
				fileIn.close();
			} catch (ClassNotFoundException io) {
				System.out.println(io.getMessage());
			} catch (IOException io) {
				System.out.println(io.getMessage());
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
			System.out.println();
			System.out.println(readed);
		} catch (IncorrectZipCodeLengthException ex) {
			System.out.println(ex.getMessage());
		} catch (Exception ex) {
			System.err.println("Unexpected exception: " + ex.getMessage());
		}

		AddressesBook bookLib = new AddressesBook();

		bookLib = SaveLoadXML.load("file.xml");
		SaveLoadXML.save("data.xml", bookLib);

	}

}
