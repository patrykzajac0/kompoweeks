
public class IncorrectZipCodeLengthException extends Exception {
	private static final long serialVersionUID = 7091527668169723624L;

	public IncorrectZipCodeLengthException(String message){
		super(message);
	}
}
