
public interface ZipCode {
	void setZipCode(String zipCode) throws IncorrectZipCodeLengthException;
	String getZipCode();
}
