import java.util.Arrays;

public class AddressesBook {
	private Address[] addresses;

	public AddressesBook(int size) {
		addresses = new Address[size];
	}

	public Address[] getAddresses() {
		return addresses;
	}

	public void setAddresses(Address[] addresses) {
		this.addresses = addresses;
	}

	public void add(Address address, int pos) throws Exception {
		if (pos >= addresses.length) {
			throw new IndexOutOfBoundsException("Index out of bounds");
		}
		addresses[pos] = address;
	}

	public Address get(int pos) {
		return addresses[pos];
	}

	public void sort(String field) {
		switch (field) {
		case "id": {
			Arrays.sort(addresses, new CompareId());
			// Arrays.sort(addresses, (a, b) -> a.getId() < b.getId() ? -1 : a.getId() ==
			// b.getId() ? 0 : 1);
		}
		case "street": {
			Arrays.sort(addresses, new CompareStreet());
			// Arrays.sort(addresses, (a, b) ->
			// a.getStreet().compareToIgnoreCase(b.getStreet()));
		}
		case "number": {
			Arrays.sort(addresses, new CompareNumber());
			// Arrays.sort(addresses, (a, b) ->
			// a.getNumber().compareToIgnoreCase(b.getNumber()));
		}
		case "zipCode": {
			Arrays.sort(addresses, new CompareZipCode());
			// Arrays.sort(addresses, (a, b) ->
			// a.getZipCode().compareToIgnoreCase(b.getZipCode()));
		}
		case "city": {
			Arrays.sort(addresses, new CompareCity());
			// Arrays.sort(addresses, (a, b) ->
			// a.getCity().compareToIgnoreCase(b.getCity()));
		}
		}
		// return addresses;
	}

}
