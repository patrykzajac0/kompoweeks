
public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int size = 10;
		AddressesBook book = new AddressesBook(size);

		for (int i = 0; i < size; i++) {
			Address temp = new Address("Street", Integer.toString(i % 3), "ZipCode", "City");
			try {
				book.add(temp, i);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		book.sort("number");

		for (int i = 0; i < size; i++) {
			System.out.println(book.get(i).toString());
		}
	}
}
