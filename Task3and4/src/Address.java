
public class Address implements Comparable<Address> {
	private long id;
	private String street;
	private String number;
	private String zipCode;
	private String city;

	public Address(String street, String number, String zipCode, String city) {
		this.id = 0;
		this.street = street;
		this.number = number;
		this.zipCode = zipCode;
		this.city = city;
	}

	public Address(Address address) {
		this.id = 0;
		this.street = address.street;
		this.number = address.number;
		this.zipCode = address.zipCode;
		this.city = address.city;
	}

	public Address(Object object) {
		if (object instanceof Address) {
			this.id = 0;
			this.street = ((Address) object).getStreet();
			this.number = ((Address) object).getNumber();
			this.zipCode = ((Address) object).getZipCode();
			this.city = ((Address) object).getCity();
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", number=" + number + ", zipCode=" + zipCode + ", city="
				+ city + "]";
	}

	@Override
	public int compareTo(Address element) {
		if (this.id > element.id) {
			return 1;
		} else if (this.id == element.id) {
			if (this.street.compareTo(element.street) == 0) {
				if (this.number.compareTo(element.number) == 0) {
					if (this.zipCode.compareTo(element.zipCode) == 0) {
						return this.city.compareTo(element.city);
					} else {
						return this.zipCode.compareTo(element.zipCode);
					}
				} else {
					return this.number.compareTo(element.number);
				}

			} else {
				return this.street.compareTo(element.street);
			}
		} else {
			return -1;
		}
	}

}
