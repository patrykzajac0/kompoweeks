import java.util.Comparator;

public class CompareZipCode implements Comparator<Address> {

	@Override
	public int compare(Address o1, Address o2) {
		// TODO Auto-generated method stub
		return o1.getZipCode().compareTo(o2.getZipCode());
	}

}
